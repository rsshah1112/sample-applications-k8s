package com.example.sample.application.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleApplicationController {

	@RequestMapping(value = "/health", method = RequestMethod.GET)
	public ResponseEntity<?> health(){
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
